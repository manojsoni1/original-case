package com.afkl.cases.df.model;

import java.util.List;

/**
 * Created by 1261432 on 6/16/2018.
 */

public class SearchCrudStatus extends AbstractCrudStatus{

   List<Location> locationList;

    public List<Location> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<Location> locationList) {
        this.locationList = locationList;
    }
}
