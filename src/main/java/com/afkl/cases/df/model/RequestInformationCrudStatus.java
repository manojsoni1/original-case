package com.afkl.cases.df.model;

/**
 * Created by 1261432 on 6/17/2018.
 */
public class RequestInformationCrudStatus extends AbstractCrudStatus {

    RequestInformation requestInformation;

    public RequestInformation getRequestInformation() {
        return requestInformation;
    }

    public void setRequestInformation(RequestInformation requestInformation) {
        this.requestInformation = requestInformation;
    }
}
