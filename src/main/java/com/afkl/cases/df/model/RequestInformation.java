package com.afkl.cases.df.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestInformation {
    @JsonProperty("totalNumberofReq")
    private long totalNumberofReq;
    @JsonProperty("successfullReqs")
    private long successfullReqs;
    @JsonProperty("failureReqsWith4xx")
    private long failureReqsWith4xx;
    @JsonProperty("failureReqsWith5xx")
    private long failureReqsWith5xx;
    @JsonProperty("currentReqProcessTime")
    @JsonIgnore
    private long currentReqProcessTime;
    @JsonProperty("cummulativeReqProcessTime")
    @JsonIgnore
    private long cummulativeReqProcessTime;
    @JsonProperty("avgReqProcessTime")
    private long avgReqProcessTime;
    @JsonProperty("leastReqProcessTime")
    private long leastReqProcessTime;
    @JsonProperty("maxReqProcessTime")
    private long maxReqProcessTime;


    public RequestInformation() {
    }

    public RequestInformation(long totalNumberofReq, long successfullReqs, long failureReqsWith4xx, long failureReqsWith5xx, long currentReqProcessTime, long cummulativeReqProcessTime, long leastReqProcessTime, long maxReqProcessTime) {
        this.totalNumberofReq = totalNumberofReq;
        this.successfullReqs = successfullReqs;
        this.failureReqsWith4xx = failureReqsWith4xx;
        this.failureReqsWith5xx = failureReqsWith5xx;
        this.currentReqProcessTime = currentReqProcessTime;
        this.cummulativeReqProcessTime = cummulativeReqProcessTime;
        this.leastReqProcessTime = leastReqProcessTime;
        this.maxReqProcessTime = maxReqProcessTime;
    }

    public long getTotalNumberofReq() {
        return totalNumberofReq;
    }

    public void setTotalNumberofReq(long totalNumberofReq) {
        this.totalNumberofReq = totalNumberofReq;
    }

    public long getSuccessfullReqs() {
        return successfullReqs;
    }

    public void setSuccessfullReqs(long successfullReqs) {
        this.successfullReqs = successfullReqs;
    }

    public long getFailureReqsWith4xx() {
        return failureReqsWith4xx;
    }

    public void setFailureReqsWith4xx(long failureReqsWith4xx) {
        this.failureReqsWith4xx = failureReqsWith4xx;
    }

    public long getFailureReqsWith5xx() {
        return failureReqsWith5xx;
    }

    public void setFailureReqsWith5xx(long failureReqsWith5xx) {
        this.failureReqsWith5xx = failureReqsWith5xx;
    }

    public long getCurrentReqProcessTime() {
        return currentReqProcessTime;
    }

    public void setCurrentReqProcessTime(long currentReqProcessTime) {
        this.currentReqProcessTime = currentReqProcessTime;
    }

    public long getCummulativeReqProcessTime() {
        return cummulativeReqProcessTime;
    }

    public void setCummulativeReqProcessTime(long cummulativeReqProcessTime) {
        this.cummulativeReqProcessTime = cummulativeReqProcessTime;
    }

    public long getAvgReqProcessTime() {
        return avgReqProcessTime;
    }

    public void setAvgReqProcessTime(long avgReqProcessTime) {
        this.avgReqProcessTime = avgReqProcessTime;
    }

    public long getLeastReqProcessTime() {
        return leastReqProcessTime;
    }

    public void setLeastReqProcessTime(long leastReqProcessTime) {
        this.leastReqProcessTime = leastReqProcessTime;
    }

    public long getMaxReqProcessTime() {
        return maxReqProcessTime;
    }

    public void setMaxReqProcessTime(long maxReqProcessTime) {
        this.maxReqProcessTime = maxReqProcessTime;
    }
}
