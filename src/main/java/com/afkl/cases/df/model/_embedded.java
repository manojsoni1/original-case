package com.afkl.cases.df.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Created by 1261432 on 6/17/2018.
 */
public class _embedded {

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    @JsonProperty("locations")
    List<Location> locations;

}
