package com.afkl.cases.df.model;

/**
 * Created by 1261432 on 6/16/2018.
 */
public class FareCrudStatus extends AbstractCrudStatus{

    Fare fare;
    public Fare getFare() {
        return fare;
    }
    public void setFare(Fare fare) {
        this.fare = fare;
    }
}
