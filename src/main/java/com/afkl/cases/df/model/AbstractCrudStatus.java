package com.afkl.cases.df.model;

import org.springframework.http.HttpStatus;

/**
 * Created by 1261432 on 5/11/2018.
 */

public class AbstractCrudStatus {

    private boolean success;

    private HttpStatus httpStatus;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
