package com.afkl.cases.df.controller;

import com.afkl.cases.df.model.RequestInformationCrudStatus;
import com.afkl.cases.df.model.SearchCrudStatus;
import com.afkl.cases.df.service.IFareService;
import com.afkl.cases.df.service.IRequestInformation;
import com.afkl.cases.df.service.ISearchService;
import com.afkl.cases.df.util.IMessageSource;
import com.afkl.cases.df.model.FareCrudStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by 1261432 on 6/16/2018.
 */
@RestController
public class CaseController implements IMessageSource {

    @Autowired
    IFareService iFareService;
    @Autowired
    ISearchService iSearchService;
    @Autowired
    IRequestInformation iRequestInformation;

    @GetMapping(value = "/distanceFare/{sourceCd}/{destinationCd}")
    public ResponseEntity<FareCrudStatus> getFare(@PathVariable("sourceCd") String sourceCd,
                                                  @PathVariable("destinationCd") String destinationCd) throws Exception {
        ResponseEntity<FareCrudStatus> responseEntity = null;
        FareCrudStatus fareCrudStatus = iFareService.getFare(sourceCd, destinationCd);
        responseEntity = new ResponseEntity<FareCrudStatus>(fareCrudStatus, fareCrudStatus.getHttpStatus());
        return responseEntity;
    }

    @GetMapping(value = "/search")
    public ResponseEntity<SearchCrudStatus> search(@RequestParam("term") String term) {
        ResponseEntity<SearchCrudStatus> responseEntity = null;
        SearchCrudStatus searchCrudStatus = iSearchService.search(term);
        responseEntity = new ResponseEntity<SearchCrudStatus>(searchCrudStatus, searchCrudStatus.getHttpStatus());
        return responseEntity;
    }

    @GetMapping(value = "/dashboardReport")
    public ResponseEntity<RequestInformationCrudStatus> getReqInfo() throws Exception {
        ResponseEntity<RequestInformationCrudStatus> responseEntity = null;
        RequestInformationCrudStatus requestInformationCrudStatus = iRequestInformation.getReqInfo();
        responseEntity = new ResponseEntity<RequestInformationCrudStatus>(requestInformationCrudStatus,requestInformationCrudStatus.getHttpStatus());
        return responseEntity;
    }
}