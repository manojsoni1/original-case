package com.afkl.cases.df.filter;

import com.afkl.cases.df.model.RequestInformation;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by 1261432 on 6/17/2018.
 */

@Service
public class RequestInformationFilter extends OncePerRequestFilter {
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    Environment environment;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        if(request.getRequestURI().contains("distanceFare")){
            long entryTime = System.currentTimeMillis();
            filterChain.doFilter(request, response);
            File file = new ClassPathResource(environment.getProperty("req.info.path")).getFile();
            RequestInformation reqInfo = objectMapper.readValue(file,RequestInformation.class);
            reqInfo.setTotalNumberofReq(reqInfo.getTotalNumberofReq()+1);
            if(response.getStatus()>=200 && response.getStatus()<400)
                reqInfo.setSuccessfullReqs(reqInfo.getSuccessfullReqs()+1);
            if(response.getStatus()>=400 && response.getStatus()<500)
                reqInfo.setFailureReqsWith4xx(reqInfo.getFailureReqsWith4xx()+1);
            if(response.getStatus()>=500)
                reqInfo.setFailureReqsWith5xx(reqInfo.getFailureReqsWith5xx()+1);
            reqInfo.setCurrentReqProcessTime(System.currentTimeMillis()- entryTime);
            reqInfo.setCummulativeReqProcessTime(reqInfo.getCummulativeReqProcessTime()+reqInfo.getCurrentReqProcessTime());
            reqInfo.setAvgReqProcessTime(reqInfo.getCummulativeReqProcessTime()/reqInfo.getTotalNumberofReq());
            if(reqInfo.getLeastReqProcessTime()==0 || reqInfo.getCurrentReqProcessTime()<reqInfo.getLeastReqProcessTime())
                reqInfo.setLeastReqProcessTime(reqInfo.getCurrentReqProcessTime());
            if(reqInfo.getCurrentReqProcessTime()>reqInfo.getMaxReqProcessTime())
                reqInfo.setMaxReqProcessTime(reqInfo.getCurrentReqProcessTime());
            objectMapper.writeValue(file, reqInfo);
        }else
            filterChain.doFilter(request, response);
    }
}
