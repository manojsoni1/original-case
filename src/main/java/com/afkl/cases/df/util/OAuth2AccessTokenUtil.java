package com.afkl.cases.df.util;

import com.afkl.cases.df.model.CrudStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.stereotype.Service;

/**
 * Created by 1261432 on 6/17/2018.
 */
@Service
public class OAuth2AccessTokenUtil {
    @Autowired
    Environment environment;

    public CrudStatus getOAuth2AccessToken(){
        CrudStatus crudStatus = new CrudStatus();
        ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
        resource.setAccessTokenUri(environment.getProperty("access_token_uri"));
        resource.setClientId(environment.getProperty("client_id"));
        resource.setClientSecret(environment.getProperty("client_secret"));
        resource.setGrantType(environment.getProperty("grant_type"));
        try {
            OAuth2RestTemplate template = new OAuth2RestTemplate(resource);
            org.springframework.security.oauth2.common.OAuth2AccessToken token = template.getAccessToken();
            crudStatus.setToken(token.getValue());
            crudStatus.setSuccess(true);
            crudStatus.setHttpStatus(HttpStatus.OK);
        } catch (OAuth2AccessDeniedException e) {
            e.getMessage();
            crudStatus.setSuccess(false);
            crudStatus.setHttpStatus(HttpStatus.UNAUTHORIZED);
            return crudStatus;
        }
        return crudStatus;
    }
}
