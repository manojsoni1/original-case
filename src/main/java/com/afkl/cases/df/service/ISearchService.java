package com.afkl.cases.df.service;

import com.afkl.cases.df.model.SearchCrudStatus;

/**
 * Created by 1261432 on 6/16/2018.
 */
public interface ISearchService {
    SearchCrudStatus search(String term);

}
