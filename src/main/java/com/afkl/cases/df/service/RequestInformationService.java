package com.afkl.cases.df.service;

import com.afkl.cases.df.model.RequestInformation;
import com.afkl.cases.df.model.RequestInformationCrudStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * Created by 1261432 on 6/16/2018.
 */

@Service
public class RequestInformationService implements IRequestInformation{
    @Autowired
    Environment environment;
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public RequestInformationCrudStatus getReqInfo(){
        RequestInformationCrudStatus requestInformationCrudStatus = new RequestInformationCrudStatus();
        try {
            File file = new ClassPathResource(environment.getProperty("req.info.path")).getFile();
            requestInformationCrudStatus.setRequestInformation(objectMapper.readValue(file, RequestInformation.class));
            requestInformationCrudStatus.setSuccess(true);
            requestInformationCrudStatus.setHttpStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.getMessage();
            requestInformationCrudStatus.setSuccess(false);
            requestInformationCrudStatus.setHttpStatus(HttpStatus.EXPECTATION_FAILED);
            return requestInformationCrudStatus;
        }
        return requestInformationCrudStatus;
    }
}
