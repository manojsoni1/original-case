package com.afkl.cases.df.service;

import com.afkl.cases.df.model.FareCrudStatus;

/**
 * Created by 1261432 on 6/16/2018.
 */
public interface IFareService {
    FareCrudStatus getFare(String source, String destination);

}
