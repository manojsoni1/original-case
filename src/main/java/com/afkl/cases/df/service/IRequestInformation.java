package com.afkl.cases.df.service;

import com.afkl.cases.df.model.RequestInformationCrudStatus;

/**
 * Created by 1261432 on 6/16/2018.
 */

public interface IRequestInformation {
    RequestInformationCrudStatus getReqInfo();
}
