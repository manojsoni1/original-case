package com.afkl.cases.df.service;

import com.afkl.cases.df.model.*;
import com.afkl.cases.df.util.OAuth2AccessTokenUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 1261432 on 6/16/2018.
 */

@Service
public class SeachService implements ISearchService {

    @Autowired
    Environment environment;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    HttpHeaders httpHeaders;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    OAuth2AccessTokenUtil auth2AccessTokenUtil;

    @Override
    public SearchCrudStatus search(String term) {
        SearchCrudStatus searchCrudStatus = new SearchCrudStatus();
        CrudStatus crudStatus = auth2AccessTokenUtil.getOAuth2AccessToken();
        if(crudStatus.isSuccess()){
            Map<String, String> requestMap = new HashMap<>();
            requestMap.put(environment.getProperty("term"), term);
            requestMap.put(environment.getProperty("access_token"), crudStatus.getToken());
            ResponseEntity<?> responseEntity = null;
            HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
            try {
                responseEntity = restTemplate.exchange(environment.getProperty("mock.get.url.search"), HttpMethod.GET, httpEntity, String.class, requestMap);
                if (responseEntity != null && responseEntity.getStatusCode().value() == HttpStatus.OK.value()) {
                        String body = responseEntity.getBody().toString();
                        SearchResult searchResult= objectMapper.readValue(body,SearchResult.class);
                        searchCrudStatus.setLocationList(searchResult.get_embedded().getLocations());
                        searchCrudStatus.setSuccess(true);
                        searchCrudStatus.setHttpStatus(responseEntity.getStatusCode());
                        return searchCrudStatus;
                } else {
                    searchCrudStatus.setSuccess(false);
                    searchCrudStatus.setHttpStatus(responseEntity.getStatusCode());
                }
            } catch (HttpClientErrorException e) {
                searchCrudStatus.setSuccess(false);
                searchCrudStatus.setHttpStatus(e.getStatusCode());
                return searchCrudStatus;
            }catch (IOException e) {
                e.printStackTrace();
                searchCrudStatus.setSuccess(false);
                searchCrudStatus.setHttpStatus(HttpStatus.EXPECTATION_FAILED);
                return searchCrudStatus;
            }
        }else {
            searchCrudStatus.setSuccess(false);
            searchCrudStatus.setHttpStatus(crudStatus.getHttpStatus());
            return searchCrudStatus;
        }
        return searchCrudStatus;

    }
}
