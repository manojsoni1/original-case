package com.afkl.cases.df.service;

import com.afkl.cases.df.model.CrudStatus;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.FareCrudStatus;
import com.afkl.cases.df.util.OAuth2AccessTokenUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 1261432 on 6/16/2018.
 */

@Service
public class FareService implements IFareService {

    @Autowired
    Environment environment;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    HttpHeaders httpHeaders;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    OAuth2AccessTokenUtil auth2AccessTokenUtil;

    @Override
    public FareCrudStatus getFare(String sourceCd, String destinationCd) {
        FareCrudStatus fareCrudStatus = new FareCrudStatus();

        if (sourceCd.equalsIgnoreCase(destinationCd)) {
            fareCrudStatus.setSuccess(false);
            fareCrudStatus.setHttpStatus(HttpStatus.UNPROCESSABLE_ENTITY);
            return fareCrudStatus;
        }

        CrudStatus crudStatus = auth2AccessTokenUtil.getOAuth2AccessToken();
        if (crudStatus.isSuccess()) {
            Map<String, String> requestMap = new HashMap<>();
            requestMap.put(environment.getProperty("sourceCd"), sourceCd);
            requestMap.put(environment.getProperty("destinationCd"), destinationCd);
            requestMap.put(environment.getProperty("access_token"), crudStatus.getToken());

            ResponseEntity<?> responseEntity = null;
            HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
            try {
                responseEntity = restTemplate.exchange(environment.getProperty("mock.get.url.fare"), HttpMethod.GET, httpEntity, String.class, requestMap);
                if (responseEntity != null && responseEntity.getStatusCode().value() == HttpStatus.OK.value()) {
                    String body = responseEntity.getBody().toString();
                    Fare fare = objectMapper.readValue(body, Fare.class);
                    fareCrudStatus.setFare(fare);
                    fareCrudStatus.setSuccess(true);
                    fareCrudStatus.setHttpStatus(responseEntity.getStatusCode());
                    return fareCrudStatus;
                } else {
                    fareCrudStatus.setSuccess(false);
                    fareCrudStatus.setHttpStatus(responseEntity.getStatusCode());
                }
            } catch (HttpClientErrorException e) {
                fareCrudStatus.setSuccess(false);
                fareCrudStatus.setHttpStatus(e.getStatusCode());
                return fareCrudStatus;
            } catch (IOException e) {
                e.printStackTrace();
                fareCrudStatus.setSuccess(false);
                fareCrudStatus.setHttpStatus(HttpStatus.EXPECTATION_FAILED);
                return fareCrudStatus;
            }
        } else {
            fareCrudStatus.setSuccess(false);
            fareCrudStatus.setHttpStatus(crudStatus.getHttpStatus());
            return fareCrudStatus;
        }
        return fareCrudStatus;
    }
}