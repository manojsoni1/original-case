package com.afkl.cases.df.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import javax.security.sasl.AuthenticationException;
import java.io.IOException;

public class AdaptorResponseErrorHandler implements ResponseErrorHandler {

    @Autowired
    private MessageSource messageSource;
    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        if (clientHttpResponse.getStatusCode() != HttpStatus.OK) {
            if (clientHttpResponse.getStatusCode() == HttpStatus.FORBIDDEN) {}
            return true;
        }
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
        if (clientHttpResponse.getStatusCode() == HttpStatus.FORBIDDEN) {
            throw new AuthenticationException();
        }
    }
}